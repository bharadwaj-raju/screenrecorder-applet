import QtQuick 2.8
import QtQuick.Layouts 1.1

import org.kde.plasma.core 2.0 as PlasmaCore
import org.kde.plasma.components 3.0 as PlasmaComponents3
import org.kde.plasma.extras 2.0 as PlasmaExtras
import org.kde.kirigami 2.4 as Kirigami

PlasmaExtras.Representation {
    id: expandedRepresentation

    Layout.minimumWidth: PlasmaCore.Units.gridUnit * 14
    Layout.minimumHeight: PlasmaCore.Units.gridUnit * 14
    Layout.preferredWidth: Layout.minimumWidth * 1.5
    Layout.preferredHeight: Layout.minimumHeight * 1.5

    collapseMarginsHint: true

    ColumnLayout {
        width: parent.width
        anchors.centerIn: parent
        Layout.alignment: Qt.AlignVCenter
        RowLayout {
            Layout.alignment: Qt.AlignHCenter
            Layout.fillWidth: true
            PlasmaComponents3.Label {
                text: i18n("Delay")
            }
            PlasmaComponents3.SpinBox {

            }
        }
        PlasmaComponents3.Button {
            Layout.alignment: Qt.AlignHCenter
            icon.name: "media-record"
            text: i18n("Record")
        }
    }
}
