import QtQuick 2.0
import org.kde.plasma.plasmoid 2.0
import org.kde.plasma.core 2.0 as PlasmaCore
import org.kde.plasma.extras 2.0 as PlasmaExtras
import org.kde.plasma.components 2.0 as PlasmaComponents
import org.kde.plasma.components 3.0 as PlasmaComponents3
import org.kde.plasma.private.screenrecorder 0.1 as ScreenRecorderPlugin

Item {
    id: root
    property string state: "init"
    property int secondsSinceRecord: 0

    ScreenRecorderPlugin.Backend {
        id: backend
    }

    Plasmoid.status: (root.state === "init") ? PlasmaCore.Types.PassiveStatus : PlasmaCore.Types.ActiveStatus
    Plasmoid.toolTipMainText: i18n("Screen Recorder")
    Plasmoid.toolTipSubText: (root.state === "init") ? i18n("Record your screen") : i18n("Recording in progress")
    Plasmoid.toolTipTextFormat: Text.PlainText
    Plasmoid.icon: "media-record"

    function toggleRecording() {
        if (root.state === "init") {
            backend.setSaveFile(backend.getAutoSaveLocation());
            backend.requestScreenSharing();
            root.state = "recording";
            return;
        } else if (root.state === "recording") {
            root.state = "init"
            backend.stopRecording();
            return;
        }
        console.log(root.state)
    }

    //Plasmoid.fullRepresentation: ExpandedRepresentation { }

    Plasmoid.compactRepresentation: PlasmaCore.IconItem {
        source: "media-record"

        active: compactMouse.containsMouse

        MouseArea {
            id: compactMouse
            anchors.fill: parent
            hoverEnabled: true
            acceptedButtons: Qt.LeftButton | Qt.MiddleButton | Qt.BackButton | Qt.ForwardButton

            onClicked: {
                switch (mouse.button) {
                    case Qt.MiddleButton:
                        root.toggleRecording()
                        break
                    default:
                        plasmoid.expanded = !plasmoid.expanded
                        break
                }
            }
        }
    }
}
