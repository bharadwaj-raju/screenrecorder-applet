#include "screenrecorderplugin.h"
#include "backend.h"

#include <QAction>

void ScreenRecorderPlugin::registerTypes(const char *uri)
{
    Q_ASSERT(QLatin1String(uri) == QLatin1String("org.kde.plasma.private.screenrecorder"));
    qmlRegisterType<Backend>(uri, 0, 1, "Backend");
}
