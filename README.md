# screenrecorder-applet

Build:

```bash
$ cmake -DCMAKE_USER_INSTALL_PREFIX:PATH=/usr -B build .
$ cd build
$ make && sudo make install
```

Then Screen Recorder should show up in system tray. Start/stop recording by middle clicking on it.
